"""
Exceptions and error codes
"""

from enum import Enum, unique


@unique
class ReturnCodes(Enum):
    UnknownError = 1
    FileError = 2
    Incomplete = 3
    IllegalContinue = 4
    IllegalRow = 5
    IllegalColumn = 6
    IllegalGame = 7
    InvalidFile = 8
    Draw = 9


class PuissanceVException(Exception):
    """Generic PuissanceV Exception"""

    def __init__(self, internal_error):
        super().__init__()
        self.internal_error = internal_error

    @property
    def error_code(self):
        """
        :return: The integer error code for this exception
        """
        return self.internal_error.value


class DrawException(PuissanceVException):
    """This exception is used when the game continue"""

    def __init__(self):
        super().__init__(ReturnCodes.Draw)


class IncompleteException(PuissanceVException):
    """
    The file conforms to the format and contains only legal moves,
    but the game is neither won nor drawn by either player and
    there are remaining available moves in the frame.
    Note that a file with only a dimension line constitutes an incomplete game
    """

    def __init__(self):
        super().__init__(ReturnCodes.Incomplete)


class IllegalContinueException(PuissanceVException):
    """
    All moves are valid in all other respects but the game
    has already been won on a previous turn so
    continued play is considered an illegal move.
    """

    def __init__(self):
        super().__init__(ReturnCodes.IllegalContinue)


class IllegalColumnException(PuissanceVException):
    """
    The file conforms to the format but contains a move for a
    column that is outside the dimensions of the board.
    i.e. the column selected is greater than X
    """

    def __init__(self, column):
        super().__init__(ReturnCodes.IllegalColumn)
        self.column = column


class IllegalRowException(PuissanceVException):
    """
    The file conforms to the format and all moves are for legal columns
    but the move is for a column that is already full due to previous moves
    """

    def __init__(self, row: int, column: int):
        super().__init__(ReturnCodes.IllegalRow)
        self.row = row
        self.column = column


class IllegalGameException(PuissanceVException):
    """
    The dimensions describe a game that can never be won
    """

    def __init__(self):
        super().__init__(ReturnCodes.IllegalGame)


class IllegalGameDimensionException(IllegalGameException):
    """
    Illegal board dimension
    """


class IllegalGameCountException(IllegalGameException):
    """
    Illegal count
    """


class FileErrorException(PuissanceVException):
    """The file can not be found, opened or read for some reason"""

    def __init__(self):
        super().__init__(ReturnCodes.FileError)


class InvalidFile(PuissanceVException):
    """The file is opened but does not conform the format"""

    def __init__(self):
        super().__init__(ReturnCodes.InvalidFile)


class GameNotInitializedException(Exception):
    """The orchestrator has not initialized the game board yet"""
