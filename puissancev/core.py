"""
Core features
"""

import logging
from array import array
from dataclasses import dataclass
from enum import Enum
from typing import List, Optional

from puissancev.exceptions import (
    DrawException,
    IllegalColumnException,
    IllegalContinueException,
    IllegalGameCountException,
    IllegalGameDimensionException,
    IllegalRowException,
    IncompleteException,
)

LOGGER = logging.getLogger(__name__)


@dataclass
class Coordinates:
    column: int
    row: int


class Axis(Enum):
    """
    Legit axis to win a game described as a column/row shift to apply to a position.
    """

    Horizontal = Coordinates(1, 0)
    Vertical = Coordinates(0, 1)
    DiagonalIncremental = Coordinates(1, 1)
    DiagonalDecremental = Coordinates(-1, 1)


class PuissanceV:
    """
    Imperative implementation of the game logic
    """

    def __init__(self, nr_columns: int, nr_rows: int, count_max: int = 4):
        """
        Build a game board
        :param nr_columns: the width of the frame;
        :param nr_rows: the height of the frame
        :param count_max: length of straight line required to win the game.
        """

        # The board is too small to be considered for a game
        if nr_columns < count_max and nr_rows < count_max:
            raise IllegalGameDimensionException

        # At least a count of two should be considered
        # for a game to be possible
        if count_max < 2:
            raise IllegalGameCountException

        self._nr_columns: int = nr_columns
        self._nr_rows: int = nr_rows
        self._count_max: int = count_max

        # The player who is playing the next move
        self._current_player: int = 0

        # The winner, if any
        self._winner: Optional[int] = None

        # This counter is used as an optimization hack
        # to avoid recomputing the whole matrix again and again
        self._nr_pieces: int = 0

        # The in-memory representation of the game board
        self._game: List[array] = [array("b") for _ in range(self._nr_columns)]

    @property
    def columns(self):
        return self._nr_columns

    @property
    def rows(self):
        return self._nr_rows

    @property
    def count(self):
        return self._count_max

    @property
    def current_player(self):
        """
        :return: the current player ID (either 0 or 1)
        """
        return self._current_player

    @property
    def winner(self):
        """
        :return: The winner id as an int (or None if any)
        """
        return self._winner

    @property
    def saturated(self) -> bool:
        """
        :return: True if all board cells has been filled
        """
        return self._nr_pieces == self._nr_rows * self._nr_columns

    def enqueue_piece(self, column: int):
        """
        Add a piece on the game board if possible
        :param column: the column to which the piece is added
        """
        LOGGER.info(f"Adding piece on column {column}")

        if column >= self._nr_columns:
            raise IllegalColumnException(column)

        column_array = self._game[column]
        position = len(column_array)

        # Will this move overflow the column?
        if position >= self._nr_rows:
            raise IllegalRowException(position, column)

        # Add the piece to the board column
        column_array.append(self._current_player)

        self._nr_pieces += 1

        # Determine if there is a winner after this move
        if self._nr_pieces >= (self._count_max * 2) - 1:
            winner = self.get_winning_player_if_any(
                Coordinates(column, position)
            )
            if winner is not None:
                self._winner = winner

        # Prepare the game for the next move (ie change current player)
        self._current_player = (self._current_player + 1) % 2
        LOGGER.debug(f"Changing current player to {self._current_player}")

    def piece_at_position(self, position: Coordinates):
        """
        :param position: the requested position
        :return: the player id piece at a specific position if any, otherwise None
        """
        try:
            return self._game[position.column][position.row]
        except IndexError:
            return None

    def check_line(self, position: Coordinates, operator: Axis) -> int:
        """
        Check if a piece is part of an axis which results in a win.
        This function could be expressed as a recursive function.
        :param position: the piece under consideration
        :param operator: the operator for the considered axis
        :return: the number of consecutive same pieces on the specified axis
        """
        operator_coord = operator.value
        count = 0

        # Color of the target piece
        expectation = self.piece_at_position(position)

        LOGGER.debug(
            f"Checking line {position}. Counting {count}/{self._count_max} (raw expectation: {expectation})"
        )

        # Each axis should be checked on both direction
        for inverse in [False, True]:
            factor = -1 if inverse else 1
            current = Coordinates(
                position.column + operator_coord.column * factor,
                position.row + operator_coord.row * factor,
            )

            while (
                # Are we inside the game board boundaries?
                0 <= current.column <= self._nr_columns
                and 0 <= current.row <= self._nr_rows
                # Are there any piece in this board slot?
                and current.column < self._nr_columns
                and current.row < len(self._game[current.column])
                # Is this entry of the same color as initial expectation?
                and expectation == self.piece_at_position(current)
                # Did we reach the required limit for winning?
                and count - 2 < self._count_max
            ):
                # Explore the axis until a valid reason to stop is met
                current.column += operator_coord.column * factor
                current.row += operator_coord.row * factor
                count += 1

        return count + 1

    def get_winning_player_if_any(self, position: Coordinates):
        """
        Checks if a specific piece is involved on a win scenario
        :param position: the piece position
        :return: the winner player if any otherwise None
        """

        # For all axis
        for operator in (
            Axis.Horizontal,
            Axis.Vertical,
            Axis.DiagonalIncremental,
            Axis.DiagonalDecremental,
        ):
            if self._winner is not None:
                raise IllegalContinueException()

            LOGGER.debug(f"Checking if player wins on axis {operator.name}")
            if self.check_line(position, operator) >= self._count_max:
                winning_player = self.piece_at_position(position)
                LOGGER.debug(f"Player {winning_player} wins!")
                return winning_player

        return None

    def raise_if_needed(self):
        """
        This method should be called at the end of the game.
        It checks if the game is coherent and raises an exception if not.
        """
        if self.winner is None:
            if self.saturated:
                raise DrawException()
            raise IncompleteException()
