"""
Live game entry-point
"""
import logging
import sys
from functools import partial
from pathlib import Path

import click
from tabulate import tabulate

from puissancev.core import Coordinates
from puissancev.exceptions import (
    IllegalGameCountException,
    IllegalGameDimensionException,
    IllegalRowException,
    IllegalColumnException,
    IncompleteException,
    PuissanceVException,
    FileErrorException,
    InvalidFile,
    ReturnCodes,
)
from puissancev.game import (
    PLAYERS_DISPLAY,
    LiveGameOrchestrator,
    FlatFileGameOrchestrator,
    RandomGameOrchestrator,
    PlaybackSpeed,
)

LOGGER = logging.getLogger(__name__)


def _draw_table(game):
    return [
        [
            PLAYERS_DISPLAY.get(game.piece_at_position(Coordinates(col, game.rows - row - 1)), "",)
            for col in range(game.columns)
        ]
        for row in range(game.rows)
    ]


def _clear_screen():
    print(chr(27) + "[2J")


@click.group()
def puissancev():
    """"""


def _play_game(orchestrator_builder):
    try:
        with orchestrator_builder() as orchestrator:
            # In case the orchestrator want to change the game
            game = orchestrator.game
            headers = range(game.columns)

            error = 0
            while True:
                table = _draw_table(game)

                if not error:
                    _clear_screen()
                    print(tabulate(table, headers, tablefmt="fancy_grid"))

                if game.winner is not None:
                    print(f"Player {PLAYERS_DISPLAY[game.winner]} won.")
                    break

                if game.saturated:
                    print(f"Game ended: draw!")
                    break

                error = 0
                try:
                    play = next(orchestrator.next_move)
                    game.enqueue_piece(play)
                    # moves.append(play)
                except IllegalRowException as e:
                    error = 1
                    print(f"Row full for columns {e.column}", file=sys.stderr)
                except IllegalColumnException as e:
                    error = 1
                    print(f"Column {e.column} is not permitted!", file=sys.stderr)
                except (StopIteration, KeyboardInterrupt):
                    # File is over but no winner...
                    game.raise_if_needed()
                    break

        sys.exit(0)
    except IllegalGameDimensionException:
        print(
            f"ERROR: Game board dimension are not describing a possible game", file=sys.stderr,
        )
    except IllegalGameCountException:
        print(
            f"ERROR: Game should consider a count > 1 to be possible", file=sys.stderr,
        )
    except IncompleteException:
        print(f"\nGame ended with no winner!", file=sys.stderr)


@puissancev.command()
@click.argument("cols", type=click.IntRange(1, 25), default=7)
@click.argument("rows", type=click.IntRange(1, 25), default=6)
@click.argument("count", type=click.IntRange(1, 25), default=4)
@click.option(
    "--record", type=click.Path(dir_okay=False), default=None, help="Produce a playback file",
)
@click.option(
    "--random", is_flag=True, default=False, help="Play a random game",
)
@click.option(
    "--speed",
    type=click.Choice(PlaybackSpeed.__members__),
    default=PlaybackSpeed.Normal.name,
    help="Playback speed for random games",
)
def live(
    cols: int = 7,
    rows: int = 6,
    count: int = 4,
    record: str = None,
    random: bool = False,
    speed: str = None,
):
    """
    Start a live game with a board size of <cols>x<rows>
    and a winning count of <count>. In case of replay,
    dimensions are overwritten by the file config.
    """
    params = [
        RandomGameOrchestrator if random else LiveGameOrchestrator,
        cols,
        rows,
        count,
        Path(record) if record else None,
    ]

    if random:
        params.append(PlaybackSpeed[speed].value)

    LOGGER.debug("Building orchestrator with: %s", params)
    _play_game(partial(*params))


@puissancev.command()
@click.argument(
    "file", type=click.Path(dir_okay=False, exists=True),
)
@click.option(
    "--speed",
    type=click.Choice(PlaybackSpeed.__members__),
    default=PlaybackSpeed.Normal.name,
    help="Playback speed",
)
def replay(file: str, speed: str = None):
    """
    Read a game recording and replay it live.
    """
    _play_game(partial(FlatFileGameOrchestrator, Path(file), PlaybackSpeed[speed].value,))


@puissancev.command()
@click.argument("file", type=click.Path(dir_okay=False, exists=True))
def check(file):
    """
    Check a game file integrity and shows either the error or the winner.
    """
    try:
        with FlatFileGameOrchestrator(Path(file)) as game_setup:
            game = game_setup.process()
            print(game.winner, end="")

        sys.exit()
    except PuissanceVException as exception:
        err_code = exception.error_code
    except IOError:
        err_code = FileErrorException().error_code
    except ValueError:
        err_code = InvalidFile().error_code

    print(
        f"ERROR: Failed with error {ReturnCodes(err_code).name} (code={err_code})",
        file=sys.stderr,
    )
    sys.exit(err_code)


def main():
    puissancev()


if __name__ == "__main__":
    main()
