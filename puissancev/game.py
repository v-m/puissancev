"""
Game/board setup
"""
import logging
import sys
from abc import ABC
from enum import unique, Enum
from pathlib import Path
from random import randint
from time import sleep
from typing import Optional

import sty

from puissancev.core import PuissanceV
from puissancev.exceptions import InvalidFile, GameNotInitializedException

LOGGER = logging.getLogger(__name__)


@unique
class PlaybackSpeed(Enum):
    Slowest = 1.5
    Slow = 1
    Normal = 0.5
    Fast = 0.25
    Fastest = 0.15


PLAYERS_DISPLAY = {
    0: f"{sty.bg.red} 0 {sty.bg.rs}",
    1: f"{sty.bg.yellow} 1 {sty.bg.rs}",
}


class GameOrchestrator(ABC):
    """
    Abstract definition of a game loading game setup.
    """

    def __init__(self):
        self._game = None

    @property
    def columns(self):
        """
        :return: the width of the frame;
        """
        return self._game.columns

    @property
    def rows(self):
        """
        :return: the height of the frame
        """
        return self._game.rows

    @property
    def win_size(self):
        """
        :return: length of straight line required to win the game
        """
        return self._game.count

    @property
    def next_move(self):
        """
        :return: Iterator over all the game moves
        """
        raise NotImplementedError

    @property
    def game(self) -> PuissanceV:
        if self._game:
            return self._game
        else:
            raise GameNotInitializedException

    def init_game(self):
        self._game = PuissanceV(self.columns, self.rows, self.win_size)
        return self._game

    def process(self):
        """
        Play the whole game until reaching a solution
        :return: the game state after all moves has been played
        """
        game = self.init_game()

        try:
            for next_moves in self.next_move:
                game.enqueue_piece(next_moves)
        except ValueError:
            raise InvalidFile

        game.raise_if_needed()
        return game


class FlatFileGameOrchestrator(GameOrchestrator):
    """
    This class orchestrate using a flat file.
    """

    def __init__(self, file: Path, delay: float = 0):
        super().__init__()
        self._file = file
        self._fp = None
        self._delay = delay

    def __enter__(self):
        self._fp = self._file.open()

        setup_line = self._fp.readline()
        self._columns, self._rows, self._win_size = map(
            int, setup_line.strip().split(" ")
        )

        self._game = PuissanceV(self._columns, self._rows, self._win_size)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._fp.close()

    @property
    def next_move(self):
        """
        Read the file line by line.
        :return:
        """
        line = self._fp.readline()

        while line:
            sleep(self._delay)
            yield int(line.strip()) - 1
            line = self._fp.readline()


class LiveGameOrchestrator(GameOrchestrator):
    """
    This class orchestrate a game playing live with keyboard
    """

    def __init__(
        self, columns: int, rows: int, count: int, file: Optional[Path] = None,
    ):
        super().__init__()
        self._file = file
        self._game = PuissanceV(columns, rows, count)
        self._fp = None
        self._last_move = None

    def __enter__(self):
        LOGGER.debug("Entering into game generator")

        if self._file:
            LOGGER.debug("Recording moves to %s", self._file)
            self._fp = self._file.open("w")
            self._fp.write(f"{self.columns} {self.rows} {self.win_size}\n")

        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self._fp:
            self._fp.close()

    def _next_move(self):
        try:
            self._last_move = int(
                input(
                    f"Player {PLAYERS_DISPLAY[self._game.current_player]}, choose a column: "
                )
            )
        except ValueError:
            print(f"Please enter a valid number", file=sys.stderr)

    @property
    def next_move(self):
        while True:
            self._next_move()
            self._write_last_move()
            yield self._last_move

    def _write_last_move(self):
        LOGGER.debug("Move recording attempt")
        if self._last_move is not None and self._fp:
            self._fp.write(f"{self._last_move + 1}\n")


class RandomGameOrchestrator(LiveGameOrchestrator):
    def __init__(
        self,
        columns: int,
        rows: int,
        count: int,
        file: Optional[Path] = None,
        delay: float = PlaybackSpeed.Normal.value,
    ):
        super().__init__(columns, rows, count, file)
        self._delay = delay

    def _next_move(self):
        sleep(self._delay)
        self._last_move = randint(0, self.game.columns - 1)
