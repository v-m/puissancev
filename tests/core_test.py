from unittest.mock import MagicMock, Mock, patch, PropertyMock

import pytest

from puissancev.core import PuissanceV
from puissancev.exceptions import (
    ReturnCodes,
    PuissanceVException,
    IllegalGameException,
    IncompleteException,
)
from puissancev.game import GameOrchestrator


@pytest.fixture()
def basic_setup(game: str):
    # def _make_basic_setup():
    setup = GameOrchestrator()
    setup._game = PuissanceV(3, 3, 3)

    with patch(
        "puissancev.game.GameOrchestrator.next_move", new_callable=PropertyMock
    ) as next_mode_mock:
        next_mode_mock.return_value = iter(map(int, game.split(" ")))
        yield setup

    # return _make_basic_setup


@pytest.mark.parametrize(
    "rows, columns, max_count",
    [
        pytest.param(1, 1, 2, id="Too small board"),
        pytest.param(10, 10, 0, id="Too small max_count"),
    ],
)
def test_minimal_viable_board(rows, columns, max_count):
    with pytest.raises(IllegalGameException):
        PuissanceV(rows, columns, max_count)


@pytest.mark.parametrize(
    "game, expected_error",
    [
        pytest.param("0 1 2 0 2 1 0 2 1", ReturnCodes.Draw, id=ReturnCodes.Draw.name),
        pytest.param("0 1 2 0", ReturnCodes.Incomplete, id=ReturnCodes.Incomplete.name),
        pytest.param(
            "0 1 0 1 0 1", ReturnCodes.IllegalContinue, id=ReturnCodes.IllegalContinue.name,
        ),
        pytest.param("0 1 1 0 0 1 1", ReturnCodes.IllegalRow, id=ReturnCodes.IllegalRow.name,),
        pytest.param("0 1 2 3", ReturnCodes.IllegalColumn, id=ReturnCodes.IllegalColumn.name,),
        pytest.param("banana", ReturnCodes.InvalidFile, id=ReturnCodes.InvalidFile.name),
    ],
)
def test_error_scenarios(expected_error, basic_setup):
    with pytest.raises(PuissanceVException) as e:
        basic_setup.process()

    assert e.value.error_code == expected_error.value


@pytest.mark.parametrize(
    "game", ["4", "-1"],
)
def test_illegal_columns(basic_setup):
    with pytest.raises(PuissanceVException) as e:
        basic_setup.process()
        assert e.value.error_code == ReturnCodes.IllegalColumn


@pytest.mark.parametrize("game", ["1 1 1 1"])
def test_illegal_row(basic_setup):
    with pytest.raises(PuissanceVException) as e:
        basic_setup.process()
        assert e.value.error_code == ReturnCodes.IllegalRow


@pytest.mark.parametrize("game", ["1 1 1"])
def test_legal_row(basic_setup):
    with pytest.raises(IncompleteException):
        basic_setup.process()


@pytest.mark.parametrize(
    "game, winner",
    [
        pytest.param("0 1 0 1 0", 0, id="Win for player 1"),
        pytest.param("0 1 2 1 0 1", 1, id="Win for player 2"),
    ],
)
def test_legit_scenario(game, winner, basic_setup):
    game = basic_setup.process()
    assert game.winner == winner


@pytest.mark.parametrize(
    "game",
    [
        pytest.param("0 0 1 1 2", id="Horizontal"),
        pytest.param("0 1 0 1 0", id="Vertical"),
        pytest.param("0 1 1 2 2 0 2", id="Diagonal (inc)"),
        pytest.param("0 0 0 1 1 1 2", id="Diagonal (dec)"),
    ],
)
def test_axis(game, basic_setup):
    game = basic_setup.process()
    assert game.winner == 0
