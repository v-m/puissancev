import pkg_resources
import pytest
from click.testing import CliRunner

from puissancev.cli import puissancev
from puissancev.exceptions import ReturnCodes


def get_test_file(file_name: str):
    return pkg_resources.resource_filename("tests.files", file_name)


@pytest.mark.parametrize(
    "file, output",
    [
        ["game3", ReturnCodes.Draw],
        ["game6", ReturnCodes.Incomplete],
        ["game7", ReturnCodes.IllegalContinue],
        ["game8", ReturnCodes.IllegalRow],
        ["game9", ReturnCodes.IllegalColumn],
        ["game10", ReturnCodes.IllegalGame],
        ["game11", ReturnCodes.InvalidFile],
        ["sdfdsfds", ReturnCodes.FileError],
    ],
)
def test_error_cases(file, output):
    runner = CliRunner()
    result = runner.invoke(puissancev, ["check", get_test_file(file)])
    assert result.exit_code == output.value


@pytest.mark.parametrize(
    "file, output", [["game1", "1"], ["game4", "0"], ["game5", "1"],],
)
def test_winning_cases(file, output):
    runner = CliRunner()
    result = runner.invoke(puissancev, ["check", get_test_file(file)])

    assert result.exit_code == 0
    assert result.stdout == output
