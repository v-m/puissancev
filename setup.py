import setuptools

setuptools.setup(
    python_requires=">=3.6",
    name="puissancev",
    version="2.0.0",
    author="Vincenzo Musco",
    author_email="dev@vincenzomusco.com",
    description="Puissance V",
    packages=setuptools.find_packages(exclude=["tests"]),
    install_requires=["click", "sty", "tabulate"],
    extras_require={"dev": ["pytest", "black"]},
    setup_requires=["pytest-runner"],
    tests_require=["pytest"],
    entry_points={"console_scripts": ["puissancev = puissancev.cli:main",]},
    include_package_data=True,
    zip_safe=False,
)
