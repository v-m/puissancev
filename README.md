# Puissance-v

Connect-4 game simulation, working with any dimensions.
The name is taken from the French name of this game (ie Puissance 4),  
where v being the unknown parameter for the dimension.

## Running modes

- Live game mode;
- Replay game mode (read old game from a file and analyze whether the game is legit or not).

## Install 

```
pip install git+https://gitlab.com/v-m/puissancev
```

or cloning the repo:

```
git clone https://gitlab.com/v-m/puissancev.git
cd puissancev
python -m venv venv
source venv/bin/activate
pip install .
```

## Run

There are several running modes that includes:

- Start a game (`live`)
- Play a random game (`live --random`)
- Record a game (`live --record record.txt`)
- Replay a game (`replay`)
- Record integrity check  (`check`)

All modes can be run using the `puissancev` command.

### Start a game

To start the game, use the `puissancev live` command.

```
Usage: puissancev live [OPTIONS] [COLS] [ROWS] [COUNT]

  Start a live game with a board size of <cols>x<rows> and a winning count
  of <count>. In case of replay, dimensions are overwritten by the file
  config.

Options:
  --record FILE                   Produce a playback file
  --random                        Play a random game
  --speed [Slowest|Slow|Normal|Fast|Fastest]
                                  Playback speed for random games
  --help                          Show this message and exit.
```

### Recordings

Recording and game files should be on the following format:

```
<COLS> <ROWS> <COUNT>
<PLAYER 0 COLUM>
<PLAYER 1 COLUM>
...
```

Indexes start at `0`. This file can then be replayed or analyzed as follows:

```
puissancev check game.txt
puissancev replay --speed Normal game.txt
```

## Tests

To run unit tests, install dev dependencies and run test discovery:

```
git clone https://gitlab.com/v-m/puissancev.git
cd puissancev
python -m venv venv
source venv/bin/activate
pip install .\[dev\]
pytest
```
